
package com.zeyn.todozeyn.zeyn;

import android.app.Activity;
        import android.app.DatePickerDialog;
        import android.app.TimePickerDialog;
        import android.content.Context;
        import android.content.DialogInterface;
        import android.content.Intent;
        import android.support.annotation.NonNull;
        import android.support.annotation.Nullable;
        import android.support.v7.app.AlertDialog;
        import android.util.Log;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ArrayAdapter;
        import android.widget.Button;
        import android.widget.CheckBox;
        import android.widget.CompoundButton;
        import android.widget.DatePicker;
        import android.widget.EditText;
        import android.widget.LinearLayout;
        import android.widget.TextView;
        import android.widget.TimePicker;

        import com.google.firebase.database.DataSnapshot;
        import com.google.firebase.database.DatabaseError;
        import com.google.firebase.database.DatabaseReference;
        import com.google.firebase.database.FirebaseDatabase;
        import com.google.firebase.database.ValueEventListener;

        import java.text.DateFormat;
        import java.text.ParseException;
        import java.text.SimpleDateFormat;
        import java.util.ArrayList;
        import java.util.Calendar;
        import java.util.Date;
        import java.util.Locale;

public class CustomAdapter extends ArrayAdapter<todo> {

    private static final String TAG = "";
    Context mContext;
    private int mResource;
    private int lastPosition = -1;
    private LayoutInflater inflater;
    private Activity parentActivity;
    private DatabaseReference mtasks;
    private DatabaseReference mDatabase;
    private DatabaseReference mtodo;
    private ArrayList<todo> list;
    String kkey=" ";
    ViewHolder holder;
    private static class ViewHolder {
        TextView ltitle;
        TextView ldue;
        CheckBox lcompleted;
        Button dbutton;
        LinearLayout tl;
        LinearLayout bl;


    }


    public CustomAdapter(@NonNull Context context, int resource, @NonNull ArrayList<todo> objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
        inflater = (LayoutInflater)mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        list=objects;
    }


    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {


        final String title = getItem(position).title;
        final String due = getItem(position).due;
        final String owner = getItem(position).owner;
        String completed = getItem(position).completed;

          todo  task = new todo(completed,due,owner,title);

        final View result;

        //ViewHolder object



        if(convertView == null){
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(mResource, parent, false);
            holder= new ViewHolder();
            holder.ltitle = (TextView) convertView.findViewById(R.id.title);

            holder.ldue = (TextView) convertView.findViewById(R.id.due);
            holder.lcompleted = (CheckBox) convertView.findViewById(R.id.checkBox);
            holder.dbutton = (Button) convertView.findViewById(R.id.delete);
            holder.bl =(LinearLayout) convertView.findViewById(R.id.bl);
            holder.tl=(LinearLayout) convertView.findViewById(R.id.tl);
            result = convertView;

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        holder.ltitle.setText(task.getTitle());
        try {
            Date date=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US).parse(due);
            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd yyyy\thh:mm a");
            String sdue  = sdf.format(date);
            holder.ldue.setText(sdue);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (completed.equals("false"))
        {
            holder.lcompleted.setChecked(false);
        }
        else holder.lcompleted.setChecked(true);

        holder.dbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               delete(title,due,owner,position);
            }
        });
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mtodo = mDatabase.child("users").child(owner).child("tasks");
        mtasks = mDatabase.child("todo");

        holder.lcompleted.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    complete(owner,true,title,due,position);

                }
                else {

                    complete(owner,false,title,due,position);

                }
            }
        });


        return convertView;

    }



    private void complete(String owner, final boolean b, final String title, final String due, final int pos) {

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mtodo = mDatabase.child("users").child(owner).child("tasks");
        mtasks = mDatabase.child("todo");
        final String tkey = null;
        mtodo.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                    final String key = childSnapshot.getKey();
                    mtasks.child(key).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            todo ta = dataSnapshot.getValue(todo.class);
                            boolean bb = false;
                            if ((ta.title.equals(title)) && (ta.due.equals(due))) {
                                kkey = key;
                                if(b)
                                {

                                    mtasks.child(kkey).child("completed").setValue("true");
                                     Main_todo.todolist.get(pos).setCompleted("true");


                                    Main_todo.customAdapter.remove(Main_todo.customAdapter.getItem(pos));

                               }
                                if(!b){

                                    mtasks.child(kkey).child("completed").setValue("false");
                                    Main_todo.todolist.get(pos).setCompleted("false");
                                    Main_todo.customAdapter.remove(Main_todo.customAdapter.getItem(pos));



                            }

                                Main_todo.customAdapter.notifyDataSetChanged();
                        }

                    }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }

        });
    }

    private void delete(final String title, final String due, String owner, final int pos) {


        mDatabase = FirebaseDatabase.getInstance().getReference();
        mtodo = mDatabase.child("users").child(owner).child("tasks");
        mtasks = mDatabase.child("todo");

        mtodo.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                    final String key = childSnapshot.getKey();
                    mtasks.child(key).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            todo ta = dataSnapshot.getValue(todo.class);
                            if ((ta.title.equals(title))&&(ta.due.equals(due)))
                            {
                                System.out.println("will delete");
                                mtodo.child(key).removeValue();
                                Main_todo.customAdapter.remove(Main_todo.customAdapter.getItem(pos));
                            }
                            mtasks.child(key).removeEventListener(this);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                }
            }



            @Override
            public void onCancelled(DatabaseError databaseError) {

            }


        });

    }


}