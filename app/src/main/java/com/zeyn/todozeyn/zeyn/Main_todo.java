package com.zeyn.todozeyn.zeyn;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ListActivity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
//import android.widget.ArrayAdapter;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
//import android.widget.ListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;


public class Main_todo extends AppCompatActivity{

      static  CustomAdapter customAdapter;
    private FirebaseAuth mAuth;
    ImageButton obutton;
    private GoogleSignInClient mGoogleSignInClient;
    private DatabaseReference mDatabase;
    private DatabaseReference mtodo;
    private DatabaseReference mtasks;
    FirebaseUser user;
    ListView list;
    private String TAG="TAG";
   // private ListView lview;
    public static ArrayList<todo> todolist = new ArrayList<>();
    private ArrayAdapter<todo> listadapter ;
    public Map<String,String> tasks_id;
    public Set<String> keys;

    @Override
    protected void onCreate(Bundle savedInstanceState ) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.todo_main);
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        list = findViewById(R.id.list_todo);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mtodo = mDatabase.child("users").child(user.getUid()).child("tasks");//.child("todo");
        mtasks= mDatabase.child("todo");

                /*getApplicationContext()*/
         customAdapter = new CustomAdapter(this,R.layout.task, todolist);
        list.setAdapter(customAdapter);
        TextView utxt=findViewById(R.id.usertxt);
        utxt.setText("Hello "+ user.getDisplayName());
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                TextView tdate = (TextView) findViewById(R.id.cdate);
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("MMM dd yyyy\nhh:mm a");
                                String dateString = sdf.format(date);
                                tdate.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();



        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        obutton = findViewById(R.id.sign_outBtn);
        mtodo.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {


                  String k= dataSnapshot.getKey();
                  mtasks.child(k).addValueEventListener(new ValueEventListener() {
                      @Override
                      public void onDataChange(DataSnapshot dataSnapshot) {
                          todo ta = dataSnapshot.getValue(todo.class);
                          todolist.add(ta);
                          customAdapter.notifyDataSetChanged();


                      }

                      @Override
                      public void onCancelled(DatabaseError databaseError) {

                      }
                  });

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                System.out.println("null");
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
               final String k = dataSnapshot.getKey();
                System.out.println(k);
                mtasks.child(k).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        todo ta = dataSnapshot.getValue(todo.class);
                        mtasks.child(k).removeValue();
                        todolist.remove(ta);
                        customAdapter.notifyDataSetChanged();


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        list.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent,
                                    View view, int position, long id) {
                todo clickedObj = (todo)parent.getItemAtPosition(position);
                update(position);
               System.out.println(clickedObj.title);
            }});


    }

    private void update(final int position) {
        final EditText taskEditText = new EditText(this);
        taskEditText.setText(todolist.get(position).getTitle());
        AlertDialog dialog = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle)
                .setTitle("Edit task")
                .setView(taskEditText)
                .setPositiveButton("next", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final String task = String.valueOf(taskEditText.getText());
                        Log.d(TAG, "Task to add: " + task);
                        updatedate(task,position);

                                        }

                                }).setNegativeButton("Cancel", null)
                                .create();
                                dialog.show();

                            }




    public  void signOut(View view) {
        // Firebase sign out
        mAuth.signOut();

        // Google sign out
        mGoogleSignInClient.signOut().addOnCompleteListener(this,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        updateUI(null);
                    }
                });
    }

    public void addtask(View view) {


        final EditText taskEditText = new EditText(this);

        AlertDialog dialog = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle)
                .setTitle("Add a new task")
                .setView(taskEditText)
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String task = String.valueOf(taskEditText.getText());
                        Log.d(TAG, "Task to add: " + task);
                           takedate(task);
                    }
                })
                .setNegativeButton("Cancel", null)
                .create();
        dialog.show();
    }




    private void updatedate(final String task, final int position)
    {
        Calendar c = Calendar.getInstance();
        int mYear, mMonth, mDay;
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        final DatePickerDialog datePickerDialog;
        datePickerDialog = new DatePickerDialog(this,new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                updatetime(task, year,monthOfYear,dayOfMonth,position);
            }
        }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }


    private void updatetime(final String task, final int year, final int monthOfYear, final int dayOfMonth, final int position)
    {

        int  mHour, mMinute;
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);
        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        Date d =new Date(year -1900, monthOfYear, dayOfMonth,hourOfDay,minute,0);
                        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
                        final String isodate= dateFormat.format(d).toString();
                        mtodo.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                                    final String key = childSnapshot.getKey();
                                    mtasks.child(key).addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            todo ta = dataSnapshot.getValue(todo.class);
                                            if ((ta.title.equals(todolist.get(position).getTitle())) && (ta.due.equals(todolist.get(position).getDue()))) {

                                                dataSnapshot.getRef().child("title").setValue(task);
                                                dataSnapshot.getRef().child("due").setValue(isodate);
                                                todolist.get(todolist.indexOf(ta)).setTitle(task);
                                                todolist.get(todolist.indexOf(ta)).setDue(isodate);
                                                customAdapter.notifyDataSetChanged();

                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }});
                                }}

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });


                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }


        private void takedate(final String task)
        {
             Calendar c = Calendar.getInstance();
            int mYear, mMonth, mDay;
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            final DatePickerDialog datePickerDialog;
            datePickerDialog = new DatePickerDialog(this,new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {
                    taketime(task, year,monthOfYear,dayOfMonth);
                }
            }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }


        private void taketime(final String task, final int year, final int monthOfYear, final int dayOfMonth)
        {

            int  mHour, mMinute;
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {
                            Date d =new Date(year -1900, monthOfYear, dayOfMonth,hourOfDay,minute,0);
                            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
                         String isodate= dateFormat.format(d).toString();
                            DatabaseReference tref= mDatabase.child("todo").push();
                            DatabaseReference uref= mDatabase.child("users").child(user.getUid().toString());


                                tref.setValue(new todo("false",isodate,user.getUid().toString(),task));

                            uref.child("tasks").child(tref.getKey()).setValue("true");

                        }
                    }, mHour, mMinute, false);
            timePickerDialog.show();
        }


    private void updateUI(FirebaseUser user) {

        if (user != null) {


            Intent intent = new Intent(this, Main_todo.class);
            startActivity(intent);
        } else {

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);

        }
    }








    }














